'use strict';

var mysql = require('mysql');
var express = require('express');
var app = express();
var fs = require("fs");
var HOST1 = process.env.HOST1 || "mysql-55-centos7";
var PWD1 = process.env.PWD1 || "mysqldb";
var USER = process.env.USER || "mysqluser";
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 9182;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

// integration to mysql server
// Hemendr: refering to the dns server ubunut1.

console.log("HOST1:" + HOST1);
console.log("PWD1:" + PWD1);
console.log("USER:" + USER);
// Hemendr updates.
var pool  = mysql.createPool({
	  connectionLimit : 10,
	  host            : HOST1,
	  user            : USER,
	  password        : PWD1
});

app.get('/listFileUsers', function (req, res) 
{	
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) 
	{
       var users = JSON.parse( data )
	   console.log( data );
       res.end( JSON.stringify(users) );
	});
});

app.get('/listFileUsers/:id', function (req, res) 
{
	   // First read existing users.
	   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) 
	   {
	      var users = JSON.parse( data );
	      var user = users["user" + req.params.id];
	      console.log( user );
	      res.end( JSON.stringify(user));
	   });
});

app.get('/listUsers', function (req, res) 
{
	console.log(">>/listUsers!");
	var result = [];
	  
	pool.getConnection(function(err, connection) 
	{
	    console.log(">> pool.getConnection");

		var sqlQuery = "select id, value from mysqldb.new_table";
	    var row;
	    try
	    {
			connection.query(sqlQuery, function (err, rows, fields) 
			{
			    console.log(">> connection.query");
			    if (err)    { throw err; }
				try 
				{
					connection.release();
				} 
				catch (e) 
				{
					throw e;
				}
				var stringRow=JSON.stringify(rows);
				var jsonRowArray = JSON.parse(stringRow);
			    console.log(jsonRowArray);
				
			    var exists = "false";
			    for (var idx = 0; idx < jsonRowArray.length; idx++)
			    {
			    	var jsonRow = jsonRowArray[idx];
				    console.log(">>id:" + jsonRow.id + " value:" + jsonRow.value + "<<");
				    var aOutRow = {"idx_x":idx, "tid_x":jsonRow.id, "tvalue_x":jsonRow.value};
		            result.push(aOutRow);		   
		            exists = "true";
			    }
			    if (exists == "false")
		    	{
			    	console.log(">>Exiting: No records found!<<");
					var faultResult = {"Error":"No Records Found"};
					res.writeHead(404, {'Content-Type': 'text/json'});
				    res.end(JSON.stringify(faultResult));			    	
		    	}
			    else
		    	{
				    console.log(JSON.stringify(result));
					res.writeHead(200, {'Content-Type': 'text/json'});
				    res.end(JSON.stringify(result));		    	
		    	}
			    console.log("<< connection.query");

			});	
	    }
	    catch(ex)
	    {
	    	console.log("Exiting: /listUsers!(Failed):" + ex);
			var faultResult = {"tid":"MySQL DB processing failed"};
			res.writeHead(404, {'Content-Type': 'text/json'});
		    res.end(JSON.stringify(faultResult));
	    }
	    console.log("<< pool.getConnection");

	});
	console.log("<< /listUsers!");
});

var server = app.listen(server_port, function () 
{
	  var host = server.address().address;
	  var port = server.address().port;
	  console.log("Example app listening at http://%s:%s", host, port);
});
